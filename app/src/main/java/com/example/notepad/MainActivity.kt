package com.example.notepad

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_zarejestruj.setOnClickListener {
            var rejestracjaIntent: Intent = Intent(applicationContext, Rejestracja::class.java)
            startActivity(rejestracjaIntent)
        }

        btn_zaloguj.setOnClickListener {
//-----------------------------------------------------------------------baza danych-----------------------------------------------------------------------
            var login = login_tv.text.toString()
            var haslo = haslo_tv.text.toString()
            val dbHelper = DataBaseHelperUzy(applicationContext)
            val db = dbHelper.writableDatabase

            val query = "SELECT * FROM ${TableInfo.TABLE_UZY_NAME} WHERE ${TableInfo.TABLE_COLUMN_UZY_LOGIN} = \"$login\""
            val cursor = db.rawQuery(query,null)

            if(cursor.moveToFirst()){
                cursor.moveToFirst()
                if(haslo == cursor.getString(2)){
                    var listaNotatekIntent: Intent = Intent(applicationContext, ListaNotatek::class.java)
                    listaNotatekIntent.putExtra("login",login)
                    startActivity(listaNotatekIntent)
                }else{
                    val niepoprawneDane = Toast.makeText(applicationContext,"Wprowadzono niepoprawne dane logowania", Toast.LENGTH_LONG)
                    niepoprawneDane.show()
                }
            }else{
                val niepoprawneDane = Toast.makeText(applicationContext,"Wprowadzono niepoprawne dane logowania", Toast.LENGTH_LONG)
                niepoprawneDane.show()
            }

            cursor.close()

//-----------------------------------------------------------------------koniec bazy danych-----------------------------------------------------------------------
        }

    }
}
