package com.example.notepad

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_lista_notatek.*
import java.io.LineNumberReader

class ListaNotatek : AppCompatActivity() {

    var login:String = "login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_notatek)

        recyclerView.layoutManager = LinearLayoutManager(this)

        val dbHelper = DataBaseHelperNote(applicationContext)
        val db = dbHelper.writableDatabase



        if (intent.hasExtra("login")){
            login=intent.getCharSequenceExtra("login").toString()
        }
        recyclerView.adapter = ListaNotatekAdapter(db, login)


        dodaj_notatke_btn.setOnClickListener {
            val dodajNotatkeIntent = Intent(applicationContext, Notatnik::class.java)
            if(intent.hasExtra("login")){
                dodajNotatkeIntent.putExtra("login",login)
            }
            startActivity(dodajNotatkeIntent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intentMainActivity = Intent(applicationContext,MainActivity::class.java)
        startActivity(intentMainActivity)
    }
}
