package com.example.notepad

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.notepadlist_row.view.*

class ListaNotatekAdapter(val db:SQLiteDatabase, val login:String): RecyclerView.Adapter<MyViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from((p0.context))
        val noteRow = layoutInflater.inflate(R.layout.notepadlist_row, p0,false)
        return MyViewHolder(noteRow)
    }

    override fun getItemCount(): Int {
        val query = "SELECT * FROM ${TableInfo.TABLE_NOTE_NAME} WHERE ${TableInfo.TABLE_COLUMN_NOTE_WLASCICIEL} = \"$login\""
        val cursor = db.rawQuery(query,null)
        val iloscWierszy = cursor.count
        cursor.close()
        return iloscWierszy
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tytul = holder.view.tytul_notatki_tv
        val zawartosc = holder.view.zawartosc_notatki
        val context: Context = holder.view.context

        
        val query = "SELECT * FROM ${TableInfo.TABLE_NOTE_NAME} WHERE ${TableInfo.TABLE_COLUMN_NOTE_WLASCICIEL} = \"$login\""
        val cursor = db.rawQuery(query,null)


        if(cursor.count>0){
            cursor.moveToPosition(position)
            tytul.text = cursor.getString(1)
            zawartosc.text = cursor.getString(2)
        }


        holder.view.setOnClickListener{
            val edycjaIntent = Intent(context,Notatnik::class.java)
            edycjaIntent.putExtra("login",login)
            edycjaIntent.putExtra("tytul",tytul.text.toString())
            edycjaIntent.putExtra("zawartosc", zawartosc.text.toString())
            edycjaIntent.putExtra("ID",getIDItem(tytul.text.toString(),zawartosc.text.toString(),login,context))
            context.startActivity(edycjaIntent)
        }

        holder.view.setOnLongClickListener(object : View.OnLongClickListener{
            override fun onLongClick(v: View?): Boolean {

                lateinit var dialog:AlertDialog
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Usuwanie notatki")
                builder.setMessage("Czy jesteś pewien, że chcesz bezpowrotnie usunąć tą notatkę?")

                val dialogClickListener = DialogInterface.OnClickListener{_,which ->
                    when(which){
                        DialogInterface.BUTTON_POSITIVE -> delete(tytul.text.toString(), zawartosc.text.toString(), login, context)
                        DialogInterface.BUTTON_NEGATIVE -> println("NO")
                    }
                }

                builder.setPositiveButton("TAK",dialogClickListener)
                builder.setNegativeButton("NIE",dialogClickListener)

                dialog = builder.create()
                dialog.show()

                return true
            }
        })
    }



    fun delete(tytul:String,zawartosc:String,wlasciciel:String, context: Context){
        var ID:String=getIDItem(tytul,zawartosc,wlasciciel,context)
        db.delete(TableInfo.TABLE_NOTE_NAME,BaseColumns._ID + "=?", arrayOf("$ID"))
        val usuniecieNotatki = Toast.makeText(context, "Notatka została usunięta!",Toast.LENGTH_LONG)
        usuniecieNotatki.show()
        val refresh = Intent(context,ListaNotatek::class.java)
        refresh.putExtra("login",login)
        context.startActivity(refresh)

    }



    fun getIDItem(tytul:String,zawartosc:String,wlasciciel:String, context: Context): String{
        val dbHelper = DataBaseHelperNote(context)
        val dbNote = dbHelper.writableDatabase
        val query = "SELECT * FROM ${TableInfo.TABLE_NOTE_NAME} WHERE ${TableInfo.TABLE_COLUMN_NOTE_TYTUL} = \"$tytul\" AND ${TableInfo.TABLE_COLUMN_NOTE_ZAWARTOSC} = \"$zawartosc\"AND ${TableInfo.TABLE_COLUMN_NOTE_WLASCICIEL} = \"$wlasciciel\""
        val cursor = db.rawQuery(query,null)

        if(cursor.moveToFirst()){
            return cursor.getString(0)
        }

        cursor.close()
        dbNote.close()

        return "false"
    }

}

class MyViewHolder(val view: View):RecyclerView.ViewHolder(view)