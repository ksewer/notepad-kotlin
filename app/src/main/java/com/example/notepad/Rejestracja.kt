package com.example.notepad

import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_rejestracja.*

class Rejestracja : AppCompatActivity() {

    var czyPoprawneHaslo1: Boolean = false
    var czyPoprawneHaslo2: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rejestracja)



        haslo_tv.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(haslo_tv.length() in 0..6){
                    poprawnoscHasla.visibility = TextView.VISIBLE
                    poprawnoscHasla.setText("Hasło za krótkie")
                    czyPoprawneHaslo1=false
                }else{
                    poprawnoscHasla.visibility = TextView.INVISIBLE
                    czyPoprawneHaslo1=true
                }
            }
        })



        powtorzHaslo_tv.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(!haslo_tv.text.toString().equals(powtorzHaslo_tv.text.toString())){
                    poprawnoscZgodnosciHasel.visibility = TextView.VISIBLE
                    poprawnoscZgodnosciHasel.setText("Hasła są różne")
                    czyPoprawneHaslo2=false
                }else{
                    poprawnoscZgodnosciHasel.visibility = TextView.INVISIBLE
                    czyPoprawneHaslo2=true
                }
            }
        })



        btn_zarejestruj.setOnClickListener {

            val login = login_tv.text.toString()
            val haslo = haslo_tv.text.toString()
            val email = email_tv.text.toString()

//-----------------------------------------------------------------------baza danych-----------------------------------------------------------------------

            val dbHelper = DataBaseHelperUzy(applicationContext)
            val db = dbHelper.writableDatabase

            val query = "SELECT * FROM ${TableInfo.TABLE_UZY_NAME} WHERE ${TableInfo.TABLE_COLUMN_UZY_LOGIN} = \"$login\""
            val cursor = db.rawQuery(query,null)

            if(cursor.count>0){
                val niepoprawneDane = Toast.makeText(applicationContext,"Uzytkownik o podanym loginie już istnieje!", Toast.LENGTH_LONG)
                niepoprawneDane.show()
            }else if(czyPoprawneHaslo1 && czyPoprawneHaslo2 && email_tv.text.toString().isNotEmpty()){
                val value = ContentValues()
                value.put("login", login)
                value.put("haslo", haslo)
                value.put("email",email)
                db.insertOrThrow(TableInfo.TABLE_UZY_NAME, null, value)
                val udanaRejestracja =
                    Toast.makeText(applicationContext, "Rejestracja przebiegła prawidłowo!", Toast.LENGTH_LONG)
                udanaRejestracja.show()
                val loginIntent: Intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(loginIntent)
            }
            else{
                val niepoprawneDane = Toast.makeText(applicationContext,"Uzupełnij poprawnie dane!", Toast.LENGTH_LONG)
                niepoprawneDane.show()
            }
            cursor.close()
//-----------------------------------------------------------------------koniec bazy danych-----------------------------------------------------------------------
        }
    }
}
