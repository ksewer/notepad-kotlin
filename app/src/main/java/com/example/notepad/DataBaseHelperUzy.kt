package com.example.notepad

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

object TableInfo: BaseColumns{
    const val TABLE_UZY_NAME = "Uzytkownicy"
    const val TABLE_COLUMN_UZY_LOGIN = "login"
    const val TABLE_COLUMN_UZY_HASLO = "haslo"
    const val TABLE_COLUMN_UZY_EMAIL = "email"
    const val TABLE_NOTE_NAME = "Notatki"
    const val TABLE_COLUMN_NOTE_TYTUL = "tytul"
    const val TABLE_COLUMN_NOTE_ZAWARTOSC = "zawartosc"
    const val TABLE_COLUMN_NOTE_WLASCICIEL = "wlasciciel"
}

object komendySQL {
    const val SQL_CREATE_TABLE_UZYTKOWNICY =
            "CREATE TABLE ${TableInfo.TABLE_UZY_NAME} (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${TableInfo.TABLE_COLUMN_UZY_LOGIN} TEXT NOT NULL," +
                    "${TableInfo.TABLE_COLUMN_UZY_HASLO} TEXT NOT NULL," +
                    "${TableInfo.TABLE_COLUMN_UZY_EMAIL} TEXT NOT NULL)"

    const val SQL_DELETE_TABLE_UZYTKOWNICY = "DROP TABLE IF EXIST ${TableInfo.TABLE_UZY_NAME}"



    const val SQL_CREATE_TABLE_NOTATKI =
        "CREATE TABLE ${TableInfo.TABLE_NOTE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${TableInfo.TABLE_COLUMN_NOTE_TYTUL} TEXT NOT NULL," +
                "${TableInfo.TABLE_COLUMN_NOTE_ZAWARTOSC} TEXT NOT NULL," +
                "${TableInfo.TABLE_COLUMN_NOTE_WLASCICIEL} TEXT NOT NULL)"

    const val SQL_DELETE_TABLE_NOTATKI = "DROP TABLE IF EXIST ${TableInfo.TABLE_NOTE_NAME}"
}

class DataBaseHelperUzy (context: Context): SQLiteOpenHelper(context, TableInfo.TABLE_UZY_NAME, null, 2) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(komendySQL.SQL_CREATE_TABLE_UZYTKOWNICY)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(komendySQL.SQL_DELETE_TABLE_UZYTKOWNICY)
        onCreate(db)
    }
}

class DataBaseHelperNote (context: Context): SQLiteOpenHelper(context, TableInfo.TABLE_NOTE_NAME, null, 2) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(komendySQL.SQL_CREATE_TABLE_NOTATKI)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(komendySQL.SQL_DELETE_TABLE_NOTATKI)
        onCreate(db)
    }
}