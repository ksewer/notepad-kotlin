package com.example.notepad

import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.BaseColumns
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_notatnik.*

class Notatnik : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notatnik)


        var tytulNotatki = tytul_notatki_et
        var zawartoscNotatki = zawartosc_notatki_et

        if(intent.hasExtra("tytul"))tytulNotatki.setText(intent.getCharSequenceExtra("tytul"))
        if(intent.hasExtra("zawartosc"))zawartoscNotatki.setText(intent.getCharSequenceExtra("zawartosc"))


        zapisz_notatke_btn.setOnClickListener {
            val login = intent.getCharSequenceExtra("login").toString()
            if(tytulNotatki.text.toString().isNotEmpty() && tytulNotatki.text.toString().isNotEmpty()){
                val dbHelper = DataBaseHelperNote(applicationContext)
                val db = dbHelper.writableDatabase

                val value = ContentValues()
                value.put("tytul", tytulNotatki.text.toString())
                value.put("zawartosc", zawartoscNotatki.text.toString())
                value.put("wlasciciel",login)

                if(intent.hasExtra("ID")){
                    val dbHelper = DataBaseHelperNote(applicationContext)
                    val dbNote = dbHelper.writableDatabase
                    dbNote.update(TableInfo.TABLE_NOTE_NAME, value,BaseColumns._ID+"=?", arrayOf(intent.getStringExtra("ID")))

                    val edycjaPoprawna = Toast.makeText(applicationContext, "Notatka została edytowana!", Toast.LENGTH_LONG)
                    edycjaPoprawna.show()
                }
                else{
                    db.insertOrThrow(TableInfo.TABLE_NOTE_NAME,null, value)

                    val dodawanieNotatkiPoprawne = Toast.makeText(applicationContext, "Notatka została dodana!", Toast.LENGTH_LONG)
                    dodawanieNotatkiPoprawne.show()
                }

                val listaNotatekIntent = Intent(applicationContext, ListaNotatek::class.java)
                listaNotatekIntent.putExtra("login",login)
                startActivity(listaNotatekIntent)
                db.close()
            }
            else{
                val dodawanieNotatkiNiepoprawne =
                    Toast.makeText(applicationContext, "Wypełnij tytuł oraz treść aby zapisać notatkę!", Toast.LENGTH_LONG)
                dodawanieNotatkiNiepoprawne.show()
            }
        }
    }
}
