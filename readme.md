# Notepad

## Informacje o aplikacji
Aplikacja umożliwia dodawanie spersonalizowanych notatek chronionych hasłem. Po założeniu konta w aplikacji, notatki, które dodasz będą widoczne tylko dla Ciebie. 

## Technologie
 * Kotlin
 * Android
 * Android Studio